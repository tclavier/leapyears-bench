package main

import "fmt"
import "strconv"
import "os"
import "bufio"

func isLeapYear(i int) bool {
	gregorianCutOverYear := 1852
	if i >= gregorianCutOverYear {
		if i%400 == 0 {
			return true
		}
		if i%100 == 0 {
			return false
		}
	}
	return i%4 == 0
}

func main() {
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		year, _ := strconv.Atoi(scanner.Text())
		fmt.Println(year, isLeapYear(year))
	}

}
