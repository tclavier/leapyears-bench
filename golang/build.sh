#!/bin/bash
WORK_DIR=$(pwd)/$(dirname "$0")
docker run --rm -it -v "${WORK_DIR}:/data" -u "$(id -u):$(id -g)" -e HOME=/data -w /data golang go build -o leapyear
