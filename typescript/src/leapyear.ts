

const gregorianCutOverYear = 1852;


export function is_leap_year(year: number): boolean {
    if (year >= gregorianCutOverYear) {
        if (year % 400 == 0)
            return true;
        if (year % 100 == 0)
            return false;
    }
    return year % 4 == 0;
}
