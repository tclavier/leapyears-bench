import { expect } from "chai";
import { is_leap_year } from "./leapyear";

describe("is leap year", () => {

  it("should not be a leap year when not divisible by four", () => {
    expect(is_leap_year(1333)).to.equal(false);
  });

  it("should be a leap year when divisible by four before 1582", () => {
    expect(is_leap_year(1300)).to.equal(true);
  });

  it("should be a leap year when divisible by 400 after 1582", () => {
    expect(is_leap_year(2000)).to.equal(true);
  });

  it("should not be a leap year when divisible by 100 after 1582", () => {
    expect(is_leap_year(1900)).to.equal(false);
  });

});
