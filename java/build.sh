#!/bin/bash
WORK_DIR=$(pwd)/$(dirname $0)
docker run --rm -it -v ${WORK_DIR}:/data -u $(id -u):$(id -g) -w /data maven:3-openjdk-15 mvn package
