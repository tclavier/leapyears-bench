package com.gitlab.tclavier.leapyears;

import java.util.Scanner;

public class LeapYears {
    public static boolean isLeapYear(int i) {
        int gregorianCutOverYear = 1852;
        if (i >= gregorianCutOverYear) {
            if (i % 400 == 0)
                return true;
            if (i % 100 == 0)
                return false;
        }
        return i % 4 == 0;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            String year = scanner.nextLine();
            System.out.println(year + " " + LeapYears.isLeapYear(Integer.parseInt(year)));
        }
        scanner.close();
    }
}
