# Objective

Test the performance of different languages on a relatively simple pure function.

# Details

Write a program that takes years on its standard input separated by new lines. This program will come out on its standard output one line per year with "true" or "false" separated by a space indicating whether the year in question is a leap year.

For example:

input : 

    1200
    1189
    1582
    1600
    1900
    2001

output :
    1200 true
    1189 false
    1582 true
    1600 true
    1900 false
    2001 false


# Leap Year algo

    boolean isLeapYear(int i) {
        int gregorianCutOverYear = 1852;
        if (i >= gregorianCutOverYear) {
            if (i % 400 == 0)
                return true;
            if (i % 100 == 0)
                return false;
        }
        return i % 4 == 0;
    }

    main(String[] args) {
        while (STDIN.hasNextLine()) {
            String year = STDIN.nextLine();
            println(year + " " + isLeapYear(year));
        }
    }

# Standards

one script `build.sh` to build the project
one script `run.sh` to run the project

each script should be executed from any directory
