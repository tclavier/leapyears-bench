#!/bin/bash
#set -x

LANGUAGES="java graalvm rust c haskell python golang dotnet"
INPUT=/tmp/stdin
MAX=500
export TIME="%e %U %S"

build_input_data() {
    [ -n "${1:-}" ] && NUMBER="${1}" || exit 1
    [ -n "${2:-}" ] && INPUT="${2}" || exit 1

    rm -rf "$INPUT"
    for i in $(seq 1 "$NUMBER")
    do 
        echo $(( RANDOM % 10000 -5000)) >> "$INPUT"
    done
}


for language in $LANGUAGES
do
    ./"$language"/build.sh
done

rm -rf /tmp/*times.dat

for nb in $(seq 1 $MAX)
do
    ITEMS=$((nb*nb))
    echo -n $ITEMS
    build_input_data $ITEMS "$INPUT"

    for language in $LANGUAGES
    do
        START=$(date +%s%N)
        ./"$language"/run.sh < "$INPUT" > /tmp/"$language"-stdout
        END=$(date +%s%N)
        sed -e 's/[t|T]rue/1/g' -e 's/[f|F]alse/0/g' /tmp/java-stdout > /tmp/ref-stdout
        sed -e 's/[t|T]rue/1/g' -e 's/[f|F]alse/0/g' /tmp/"$language"-stdout > /tmp/normalize-stdout
        diff /tmp/ref-stdout /tmp/normalize-stdout >/dev/null 2>/dev/null
        RET=$?
        if [ $RET -eq 0 ]
        then
            ELAPSED=$((END - START))
            echo -n " $language"
            echo "$ITEMS $ELAPSED" >> /tmp/"$language"-times.dat
        else
            echo -n " -$language-"
        fi
    done
    echo "."
done

gnuplot bench.gnuplot > bench.svg

