package com.gitlab.tclavier.leapyears;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class LeapYearsTest {
    @Test
    void should_not_be_a_leap_year_when_not_divisible_by_four() {
        assertFalse(LeapYears.isLeapYear(1333));
    }

    @Test
    void should_be_a_leap_year_when_divisible_by_four_before_1582() {
        assertTrue(LeapYears.isLeapYear(1300));
    }

    @Test
    void should_be_a_leap_year_when_divisible_by_400_after_1582() {
        assertTrue(LeapYears.isLeapYear(2000));
    }

    @Test
    void should_not_be_a_leap_year_when_divisible_by_100_after_1582() {
        assertFalse(LeapYears.isLeapYear(1900));
    }
}
