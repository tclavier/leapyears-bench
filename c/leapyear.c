#include <stdio.h>
#include <stdlib.h>

int isLeapYear(int year) {
    int gregorianCutOverYear = 1852;
    if (year >= gregorianCutOverYear) {
            if (year % 400 == 0)
                return 1;
            if (year % 100 == 0)
                return 0;
        }
        return year % 4 == 0;
}

int main() {
    int year;
    char *line = NULL;
    size_t size=0;

    while (getline(&line, &size, stdin)!= -1){
        sscanf(line, "%d", &year, stdin); 
        printf("%d %d\n", year, isLeapYear(year));
    }
}
