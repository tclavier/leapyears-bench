#!/bin/bash
WORK_DIR=$(pwd)/$(dirname "$0")
docker build -t gcc "$WORK_DIR"
docker run --rm -it -v "${WORK_DIR}:/data" -u "$(id -u):$(id -g)" -w /data gcc gcc leapyear.c -O3 -o leapyear
