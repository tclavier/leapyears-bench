set terminal svg size 1920,1080 dynamic 
set ylabel "Temps d'éxécution en secondes"
set xlabel "Nombre d'appels de la fonction isLeapYear"
set y2range [0:100000]
plot '/tmp/graalvm-times.dat' using 1:($2/1000000000) title "graalvm" with lines, \
     '/tmp/java-times.dat' using 1:($2/1000000000) title "java" with lines, \
     '/tmp/dotnet-times.dat' using 1:($2/1000000000) title "dotnet" with lines, \
     '/tmp/haskell-times.dat' using 1:($2/1000000000) title "haskell" with lines, \
     '/tmp/python-times.dat' using 1:($2/1000000000) title "python" with lines, \
     '/tmp/golang-times.dat' using 1:($2/1000000000) title "golang" with lines, \
     '/tmp/rust-times.dat' using 1:($2/1000000000) title "rust" with lines, \
     '/tmp/c-times.dat' using 1:($2/1000000000) title "c" with lines
