﻿using System;

namespace dotnet
{
    class Program
    {
        static bool isLeapYear(int i)
        {
            int gregorianCutOverYear = 1852;
            if (i >= gregorianCutOverYear)
            {
                if (i % 400 == 0)
                    return true;
                if (i % 100 == 0)
                    return false;
            }
            return i % 4 == 0;
        }

        static void Main(string[] args)
        {
            string line;
            while ((line = Console.ReadLine()) != null)
            {
                int year = Int16.Parse(line);
                Console.WriteLine(year + " " + isLeapYear(year));
            }
        }
    }
}
