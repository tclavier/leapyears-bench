#!/bin/bash
WORK_DIR=$(pwd)/$(dirname "$0")
docker run --rm -it -v "${WORK_DIR}":/data -u "$(id -u)":"$(id -g)" -e HOME=/data -w /data mcr.microsoft.com/dotnet/sdk dotnet build -o leapyear --configuration Release 
