module LeapYear
    ( isLeapYear
    ) where


gregorianCutOverYear :: Int
gregorianCutOverYear = 1852


isLeapYear :: Int -> Bool
isLeapYear year
    | year >= gregorianCutOverYear =
        let
            typicalLeapYear = year `rem` 4 == 0
            atypicalCommonYear = year `rem` 100 == 0
            atypicalLeapYear = year `rem` 400 == 0
        in
            atypicalLeapYear || (not atypicalCommonYear && typicalLeapYear)
    | otherwise = year `rem` 4 == 0
