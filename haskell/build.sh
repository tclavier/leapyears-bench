#!/bin/bash
WORK_DIR=$(pwd)/$(dirname $0)
docker run --rm -it -v ${WORK_DIR}:/data -w /data -u "$(id -u):$(id -g)" haskell stack --stack-root=/data --allow-different-user --install-ghc install 

