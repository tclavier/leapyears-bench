{-# LANGUAGE OverloadedStrings #-}
module LeapYearSpec (spec) where


import qualified LeapYear
import           Test.Hspec


spec :: Spec
spec = do

    it "should not be a leap year when not divisible by four" $
        LeapYear.isLeapYear 1333 `shouldBe` False

    it "should be a leap year when divisible by four before 1582" $
        LeapYear.isLeapYear 1300 `shouldBe` True

    it "should be a leap year when divisible by 400 after 1582" $
        LeapYear.isLeapYear 2000 `shouldBe` True

    it "should not be a leap year when divisible by 100 after 1582" $
        LeapYear.isLeapYear 1900 `shouldBe` False

