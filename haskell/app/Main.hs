module Main where

import qualified LeapYear


compute :: String -> String
compute s = s ++ " " ++ (show . LeapYear.isLeapYear . read) s


main :: IO ()
main = interact (unlines . map compute . lines)
