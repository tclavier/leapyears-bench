# -*- coding: utf-8 -*-
from __future__ import annotations

import sys


GREGORIAN_CUTOVER_YEAR = 1852


def is_leap_year(i: int) -> bool:
    if i >= GREGORIAN_CUTOVER_YEAR:
        if i % 400 == 0:
            return True
        if i % 100 == 0:
            return False
    return i % 4 == 0


if __name__ == "__main__":
    for line in sys.stdin:
        year = int(line.rstrip())
        print(f"{year} {is_leap_year(year)}")
