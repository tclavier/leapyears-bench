# -*- coding: utf-8 -*-

from leap_year import is_leap_year


def test_should_not_be_a_leap_year_when_not_divisible_by_four():
    assert is_leap_year(1333) is False


def test_should_be_a_leap_year_when_divisible_by_four_before_1582():
    assert is_leap_year(1300) is True


def test_should_be_a_leap_year_when_divisible_by_400_after_1582():
    assert is_leap_year(2000) is True


def test_should_not_be_a_leap_year_when_divisible_by_100_after_1582():
    assert is_leap_year(1900) is False
