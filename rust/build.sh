#!/bin/bash
WORK_DIR=$(pwd)/$(dirname "$0")
docker run --rm -it -v "${WORK_DIR}":/data -u "$(id -u)":"$(id -g)" -w /data rust cargo build --release
