use std::io::{self, BufRead};

fn is_leap_year(year: i32) -> bool {
    let gregorian_cut_over_year = 1852;
    if year >= gregorian_cut_over_year {
        if year % 400 == 0 {
            return true;
        }
        if year % 100 == 0 {
            return false;
        }
    }
    return year % 4 == 0;
}

fn main() {
    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let year = line.unwrap().parse::<i32>().unwrap();
        println!("{} {}", year, is_leap_year(year));
    }
}
